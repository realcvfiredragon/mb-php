<?php
class Page
{
    private function _formatBytes($bytes, $precision = 2)
    {
        // https://stackoverflow.com/a/2510459
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);
        $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    public function beforeRoute($f3, $args)
    {
        $f3->set('showlist',
            $f3->get('db')->exec(
                'SELECT showShortName, showRating, showName, stTitle FROM shows INNER JOIN showStatus ON shows.showStatus = showStatus.stId WHERE showIsHidden = 0 ORDER BY showPriority'
            )
        );
    }

    public function home($f3, $args)
    {
        $f3->set('latest', $f3->get('db')->exec(
            'SELECT epiNumberTotal, epiSeason, epiNumber, epiName, showShortName, showName FROM episodes INNER JOIN shows ON epiShow = showId WHERE showIsHidden=0 ORDER BY epiPublishedDate DESC LIMIT 9;'
        ));

        echo \Template::instance()->render('home.htm');
    }

    public function faq($f3, $args)
    {
        echo \Template::instance()->render('faq.htm');
    }

    public function report($f3, $args)
    {
        echo \Template::instance()->render('report.htm');
    }

    public function live($f3, $args)
    {
        echo \Template::instance()->render('live.htm');
    }

    public function stats($f3, $args)
    {
        $s = json_decode(file_get_contents(__DIR__ . '/../../assets/stats.json'), true);

        $dvis = [];
        foreach ($s['result']['timeseries'] as $d) {
            $dvis[$d['since']] = $d['uniques']['all'];
        }

        $storage = 0;
        $directory = __DIR__ . '/../../files/';
        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory)) as $file) {
            $storage += $file->getSize();
        }

        $t = $s['result']['totals'];
        arsort($t['requests']['country']);

        $sExt = file_get_contents('https://files.ctoon.party/size.txt');

        $f3->set('stats', array(
            'from' => $t['since'],
            'to' => $t['until'],
            'visitors' => $t['uniques']['all'],
            'pageviews' => $t['pageviews']['all'],
            'bandwidth' => $this->_formatBytes($t['bandwidth']['all']),
            'requests' => $t['requests']['all'],
            'topcountries' => $t['requests']['country'],
            'dvisitor' => $dvis,
            'costs' => '75USD',
            'storage' => $this->_formatBytes($storage),
            'storage_ext' => $this->_formatBytes($sExt)
        ));

        // echo json_encode($f3->get('stats'));
        echo \Template::instance()->render('stats.htm');
    }

    public function show($f3, $args)
    {
        $show = $f3->get('db')->exec(
            'SELECT *, COUNT(showId) AS count FROM shows WHERE showShortName = ? LIMIT 1',
            array(
                $f3->get('PARAMS.show'),
            )
        )[0];

        if ($show['count'] && $show['count'] == 1) {
            $f3->set('show', $show);
            $f3->set('pageTitle', $show['showName']);
            $f3->set('ogImage', 'https://' . $f3->get('SERVER.SERVER_NAME') . '/assets/img/shows/' . $show['showShortName'] . '.jpg');
            $f3->set('ogImageAlt', $show['showName']);

            $seasons = $f3->get('db')->exec(
                'SELECT epiSeason FROM episodes INNER JOIN shows ON episodes.epiShow = shows.showId WHERE showShortName = ? GROUP BY epiSeason',
                array(
                    $f3->get('PARAMS.show'),
                )
            );

            $episodes = [];

            foreach ($seasons as $season) {
                $se = $season['epiSeason'];
                $ep = $f3->get('db')->exec(
                    'SELECT * FROM episodes INNER JOIN shows ON episodes.epiShow = shows.showId WHERE showShortName = ? AND epiSeason = ? ORDER BY epiNumberTotal',
                    array(
                        $f3->get('PARAMS.show'),
                        $se,
                    )
                );
                $episodes[$se] = $ep;
            }
            $f3->set('seasons', $episodes);

            $f3->set('colDisplay', $f3->get('COOKIE.colDisplay') ? $f3->get('COOKIE.colDisplay') : 'horizontal');

            echo \Template::instance()->render('show.htm');
        } else {
            $f3->error(404);
        }
    }

    public function episode($f3, $args)
    {
        $episode = $f3->get('db')->exec(
            'SELECT *, COUNT(epiId) AS count FROM episodes INNER JOIN shows ON episodes.epiShow = shows.showId WHERE showShortName = ? AND epiNumberTotal = ? GROUP BY epiId LIMIT 1',
            array(
                $f3->get('PARAMS.show'),
                $f3->get('PARAMS.episode'),
            )
        )[0];

        if ($episode['count'] && $episode['count'] == 1) {
            $ipHash = md5($f3->get('userip'));
            $epiCode = $episode['showShortName'] . '-' . $episode['epiNumberTotal'];
            $unique = $epiCode . "-" . $ipHash;

            $f3->set('pageTitle', $episode['epiName'] . ' - ' . $episode['showName']);
            $f3->set('pageDesc', parseSE($episode['epiSeason'], $episode['epiNumber']));
            $f3->set('ogImage', 'https://' . $f3->get('SERVER.SERVER_NAME') . '/assets/img/shows/' . $episode['showShortName'] . '.jpg');
            $f3->set('ogImageAlt', $episode['showName']);

            $f3->set('prevEpisode',
                $f3->get('db')->exec(
                    'SELECT epiNumberTotal, epiSeason, epiNumber, epiName FROM episodes WHERE epiShow = ? AND epiNumberTotal < ? ORDER BY epiNumberTotal DESC',
                    array(
                        $episode['epiShow'],
                        $episode['epiNumberTotal'],
                    )
                )[0]
            );

            $f3->set('nextEpisode',
                $f3->get('db')->exec(
                    'SELECT epiNumberTotal, epiSeason, epiNumber, epiName FROM episodes WHERE epiShow = ? AND epiNumberTotal > ? ORDER BY epiNumberTotal ASC',
                    array(
                        $episode['epiShow'],
                        $episode['epiNumberTotal'],
                    )
                )[0]
            );

            $f3->set('episode', $episode);
            $f3->get('assets')->add('/node_modules/plyr/dist/plyr.css', 'css', 'head');
            $f3->get('assets')->add('/node_modules/plyr/dist/plyr.polyfilled.min.js', 'js', 'head');
            echo \Template::instance()->render('episode.htm');
        } else {
            $f3->error(404);
        }
    }

    public function error($f3, $args)
    {
        echo \Template::instance()->render('error.htm');
    }
}
