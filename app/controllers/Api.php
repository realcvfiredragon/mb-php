<?php
class Api
{
    public function beforeRoute($f3)
    {
        header("Access-Control-Allow-Origin: *", true);
        header("Content-Type: application/json", true);
    }

    public function _show($show)
    {
        return array(
            "id" => (int) $show['showId'],
            "short_name" => $show['showShortName'],
            "name" => $show['showName'],
            "rating" => $show['showRating'] ? $show['showRating'] : 'TV-Y7',
            "banner" => "/assets/img/banners/" . $show['showShortName'] . ".jpg",
            "cover" => "/assets/img/shows/" . $show['showShortName'] . ".jpg",
            "description" => $show['showDescription'],
            "description_source" => $show['showDescriptionSource'],
            "links" => array(
                "official" => $show['showLink'],
                "fan_news" => $show['showLinkFanNews'],
                "imdb" => 'http://www.imdb.com/title/' . $show['showImdb'],
                "itunes" => $show['showLinkItunes'],
                "amazon" => $show['showLinkAmazon'],
                "google_play" => $show['showLinkPlay'],
                "netflix" => (bool) $show['showIsOnNetflix'],
            ),
        );
    }

    public function _episode($episode, $extended = false)
    {
        $arr = array(
            "id" => (int) $episode['epiNumberTotal'],
            "title" => (string) $episode['epiName'],
            "season" => (string) seasonParse($episode['epiSeason']),
            "episode" => (string) $episode['epiNumber'],
            "sxe" => array(
                "short" => (string) parseSE($episode['epiSeason'], $episode['epiNumber'], 'short'),
                "long" => (string) parseSE($episode['epiSeason'], $episode['epiNumber'])
            ),
            "published_date" => (string) $episode['epiPublishedDate'],
        );

        if ($extended) {
            $base = "/files/" . $episode['showShortName'] . "/s";
            $baseVid = $base . $episode['epiSeason'] . "/Marebucks-" . $episode['showShortName'] . "-";

            $webm = array(
                "240" => $baseVid . "240p-" . $episode['epiSeason'] . "x" . $episode['epiNumber'] . ".webm",
                "480" => $baseVid . "480p-" . $episode['epiSeason'] . "x" . $episode['epiNumber'] . ".webm",
            );

            if (!$episode['epiNo720p']) {
                $webm['720'] = $baseVid . "720p-" . $episode['epiSeason'] . "x" . $episode['epiNumber'] . ".webm";
            }

            if (!$episode['epiNo1080p']) {
                $webm['1080'] = $baseVid . "1080p-" . $episode['epiSeason'] . "x" . $episode['epiNumber'] . ".webm";
            }

            $mp4 = array(
                "480" => $baseVid . "480p-" . $episode['epiSeason'] . "x" . $episode['epiNumber'] . ".mp4",
            );

            $subtitles = [];
            foreach (explode(',', $episode['epiSubs']) as $sub) {
                if ($sub) $subtitles[$sub] = $base . "ubs/" . $sub . "/" . $episode['epiSeason'] . "x" . $episode['epiNumber'] . ".vtt";
            }

            $hls = null;
            if ($episode['epiHLS']) {
                $hls = 'https://files.ctoon.party/hls/' . $episode['showShortName'] . '/en/' . $episode['showShortName'] . '-' . $episode['epiSeason'] . 'x' . $episode['epiNumber'] . '-en-webdl/playlist.m3u8';
            }

            $arr['files'] = array(
                "webm" => $webm,
                "mp4" => $mp4,
                "hls" => $hls,
                "subtitles" => $subtitles
            );
        }

        return $arr;
    }

    public function getAll($f3, $args)
    {
        $limit = new RateLimiter(300, 50);
        if (!$limit->check('apiAll-' . $f3->get('userip'))) {
            jsonError('API Limit reached for /api/all. Try again in 5 minutes.', 429);
            return;
        }

        $json['latest'] = $json['shows'] = [];

        $shows = $f3->get('db')->exec(
            'SELECT * FROM shows WHERE showIsHidden = 0 ORDER BY showPriority;'
        );
        foreach ($shows as $show) {
            $i = $show['showShortName'];
            $json['shows'][$i]['show'] = $this->_show($show);

            $episodes = $f3->get('db')->exec(
                'SELECT * FROM episodes INNER JOIN shows ON showId = epiShow WHERE epiShow = ? ORDER BY epiSeason, epiNumber;',
                $show['showId']
            );

            $episodeList = [];
            foreach ($episodes as $episode) {
                $s = seasonParse($episode['epiSeason'], true);
                if (!array_key_exists($s, $episodeList)) {
                    $episodeList[$s] = [];
                }

                array_push($episodeList[$s], $this->_episode($episode));
            }

            $json['shows'][$i]['seasons'] = $episodeList;
        }

        $episodes = $f3->get('db')->exec(
            'SELECT * FROM episodes ORDER BY epiPublishedDate DESC LIMIT 10;'
        );
        foreach ($episodes as $episode) {
            $ep['episode'] = $this->_episode($episode);

            $show = $f3->get('db')->exec(
                'SELECT * FROM shows WHERE showId = ?;',
                $episode['epiShow']
            )[0];

            $ep['show'] = $this->_show($show);

            array_push($json['latest'], $ep);
        }

        echo json_encode($json);
    }

    public function getList($f3, $args)
    {
        $limit = new RateLimiter(300, 50);
        if (!$limit->check('apiList-' . $f3->get('userip'))) {
            jsonError('API Limit reached for /api/list. Try again in 5 minutes.', 429);
            return;
        }

        $shows = $f3->get('db')->exec(
            'SELECT * FROM shows ORDER BY showId;'
        );

        $json = [];
        foreach ($shows as $show) {
            array_push($json, $this->_show($show));
        }

        echo json_encode($json);
    }

    public function getShow($f3, $args)
    {
        $limit = new RateLimiter(300, 50);
        if (!$limit->check('apiShow-' . $f3->get('userip'))) {
            jsonError('API Limit reached for /api/:show. Try again in 5 minutes.', 429);
            return;
        }

        $json = $episodeList = [];

        $show = $f3->get('db')->exec(
            'SELECT * FROM shows WHERE showShortName = ?;',
            $f3->get('PARAMS.show')
        )[0];
        $json['show'] = $this->_show($show);

        $episodes = $f3->get('db')->exec(
            'SELECT * FROM episodes INNER JOIN shows ON showId = epiShow WHERE epiShow = ? ORDER BY epiSeason, epiNumber;',
            $show['showId']
        );
        foreach ($episodes as $episode) {
            $s = seasonParse($episode['epiSeason'], true);
            if (!array_key_exists($s, $episodeList)) {
                $episodeList[$s] = [];
            }

            array_push($episodeList[$s], $this->_episode($episode));
        }

        $json['seasons'] = $episodeList;

        echo json_encode($json);
    }

    public function getEpisode($f3, $args)
    {
        $limit = new RateLimiter(300, 50);
        if (!$limit->check('apiEpisode-' . $f3->get('userip'))) {
            jsonError('API Limit reached for /api/:show/:episode. Try again in 5 minutes.', 429);
            return;
        }

        $json = [];
        $json['_prev'] = $json['_next'] = null;

        $show = $f3->get('db')->exec(
            'SELECT * FROM shows WHERE showShortName = ?;',
            $f3->get('PARAMS.show')
        )[0];
        $json['show'] = $this->_show($show);

        $episode = $f3->get('db')->exec(
            'SELECT * FROM episodes INNER JOIN shows ON showId = epiShow WHERE showShortName = ? AND epiNumberTotal = ? LIMIT 1;',
            array($f3->get('PARAMS.show'), $f3->get('PARAMS.episode'))
        )[0];
        $json['episode'] = $this->_episode($episode, true);

        $prev = $f3->get('db')->exec(
            'SELECT * FROM episodes INNER JOIN shows ON showId = epiShow WHERE showShortName = ? AND epiNumberTotal < ? ORDER BY epiNumberTotal DESC LIMIT 1',
            array($f3->get('PARAMS.show'), $f3->get('PARAMS.episode'))
        )[0];
        if ($prev) $json['_prev'] = $this->_episode($prev);

        $next = $f3->get('db')->exec(
            'SELECT * FROM episodes INNER JOIN shows ON showId = epiShow WHERE showShortName = ? AND epiNumberTotal > ? ORDER BY epiNumberTotal ASC LIMIT 1',
            array($f3->get('PARAMS.show'), $f3->get('PARAMS.episode'))
        )[0];
        if ($next) $json['_next'] = $this->_episode($next);

        echo json_encode($json);
    }
}
