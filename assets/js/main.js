/* global jQuery, document */
jQuery('.lights').click(function () {
  jQuery('body').toggleClass('lightsoff')

  var expires = new Date()
  expires.setTime(expires.getTime() + (365 * 24 * 60 * 60 * 1000))
  document.cookie = 'lightsoff=' + jQuery('body').hasClass('lightsoff') + ';expires=' + expires.toUTCString() + '; path=/'
})

jQuery('.surveys button').click(function () {
  jQuery('.surveys').remove()

  var expires = new Date()
  expires.setTime(expires.getTime() + (365 * 24 * 60 * 60 * 1000))
  document.cookie = 'survey1ok=true;expires=' + expires.toUTCString() + '; path=/'
})
